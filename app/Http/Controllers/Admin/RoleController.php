<?php
namespace App\Http\Controllers\Admin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
class RoleController extends Controller
{
    /**
     * @var Role
     */
    private $role;
    function __construct(Role $role)
    {
        $this->role = $role;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array = array();
        $roles=Role::get();
        foreach ($roles as $rol) {

            $datos = [
                'id' => $rol->id,
                'name' => $rol->name,
                'guard_name' => $rol->guard_name,
                'created_at' => date("d/m/Y h:i A", strtotime($rol->created_at)),
                'updated_at' => date("d/m/Y h:i A", strtotime($rol->updated_at)),
                'permisos' => $rol->permissions()->get(),
            ];

            if(Auth::user()->roles[0]['name']=='Super Admin' && $rol->name=='Super Admin'){
                array_push($array, $datos);
            }
            if($rol->name!='Super Admin'){
                array_push($array, $datos);
            }
        }
        $response = array(
            'status' => 'success',
            'msg' => $array,
        );

        return response()->json($response);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'name'=>'required',
        ]);
        $role= $this->role->create([
            'name'=> $request->name
        ]);
        if ($request->has('permissions')) {
            $role->givePermissionTo($request->permissions);
        }
        return response(['message'=>'Rol Creado']);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Role $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $request->validate([
            'name'=>'required',
        ]);
        $role->update([
            'name'=> $request->name,
        ]);
        if ($request->has('permissions')) {
            $role->syncPermissions($request->permissions);
        }
        return response(['message'=>'Rol Actualizado']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->role->destroy($id);
        return response(['message'=>'Rol Eliminado']);
    }
}