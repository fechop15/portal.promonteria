<?php

namespace App\Http\Controllers;

use App\empresas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $response = array(
            'status' => 'success',
            'msg' => Empresas::where('activo', true)->get()
        );


        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request, [
            'razonSocial' => 'required|string',
            'nit' => 'required|string|unique:empresas,nit,' . $request->nit,
            'telefono' => 'required|string',
            'city_id' => 'required|numeric',
            'direccionEmpresa' => 'required|string',
            'representante' => 'required|string',
            'anio' => 'required|numeric',
            'empleados' => 'required|numeric',
            'sector' => 'required|string',
            'actividadPrincipal' => 'required|string',
            'actividadSecundaria' => 'required|string',
            'promedioVentas' => 'required|string',
            'totalActivos' => 'required|string',
            'operacionesInternacionales' => 'required|string',
            'tipoOperaciones' => 'required|string',
            'tieneSucursales' => 'required|string',


        ], [
            'nit.unique' => 'Este Documento ya existe en la base de datos, intente con otro',
            'razonSocial.required' => 'razonSocial Requerido',
            'telefono.required' => 'telefono Requerido',
            'city_id.required' => 'city_id Requerido',
            'direccionEmpresa.required' => 'direccionEmpresa Requerido',
            'representante.required' => 'representante Requerido',
            'anio.required' => 'anio Requerido',
            'empleados.required' => 'empleados Requerido',
            'sector.required' => 'sector Requerido',
            'actividadPrincipal.required' => 'actividadPrincipal Requerido',
            'actividadSecundaria.required' => 'actividadSecundaria Requerido',
            'promedioVentas.required' => 'promedioVentas Requerido',
            'totalActivos.required' => 'totalActivos Requerido',
            'operacionesInternacionales.required' => 'operacionesInternacionales Requerido',
            'tipoOperaciones.required' => 'tipoOperaciones Requerido',
            'tieneSucursales.required' => 'tieneSucursales Requerido',
        ]);

        try {
            $error = null;
            DB::beginTransaction();

            $path = "";
            $filename = "";

            if ($request->uploadImg != null) {
                // Subimos el logo alservidor
                $path = "images/logoEmpresa/";
                $filename = $request->nit . '.' . $request->uploadImg->getClientOriginalExtension();
                move_uploaded_file($request->file('uploadImg'), $path . $filename);
                //
            }

            $empresa = Empresas::create([
                'razonSocial' => strtoupper($request->razonSocial),
                'nit' => $request->nit,
                'telefono' => $request->telefono,
                'city_id' => $request->city_id,
                'direccionEmpresa' => strtoupper($request->direccionEmpresa),
                'representante' => strtoupper($request->representante),
                'anio' => $request->anio,
                'empleados' => $request->empleados,
                'sector' => $request->sector,
                'actividadPrincipal' => $request->actividadPrincipal,
                'actividadSecundaria' => $request->actividadSecundaria,
                'promedioVentas' => $request->promedioVentas,
                'totalActivos' => $request->totalActivos,
                'operacionesInternacionales' => $request->operacionesInternacionales,
                'tipoOperaciones' => $request->tipoOperaciones,
                'tieneSucursales' => $request->tieneSucursales,
                'sucursales' => $request->sucursales,
                'comentarios' => $request->comentarios,
                'web' => $request->web,
                'portafolio' => $request->portafolio,
                'logo' => $path . $filename

            ]);

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => $empresa,
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,

            );
        }//error


        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function actualizarEmpresa(Request $request)
    {

        $usuario = Empresas::findOrFail($request->id);


        $error = null;
        DB::beginTransaction();
        try {

            $this->validate($request, [
                'razonSocial' => 'required|string',
                'nit' => 'required|string',
                'telefono' => 'required|string',
                'city_id' => 'required|numeric',
                'direccionEmpresa' => 'required|string',
                'representante' => 'required|string',
                'anio' => 'required|numeric',
                'empleados' => 'required|numeric',
                'sector' => 'required|string',
                'actividadPrincipal' => 'required|string',
                'actividadSecundaria' => 'required|string',
                'promedioVentas' => 'required|string',
                'totalActivos' => 'required|string',
                'operacionesInternacionales' => 'required|string',
                'tipoOperaciones' => 'required|string',
                'tieneSucursales' => 'required|string',


            ], [
                'nit.unique' => 'Este Documento ya existe en la base de datos, intente con otro',
                'razonSocial.required' => 'razonSocial Requerido',
                'telefono.required' => 'telefono Requerido',
                'city_id.required' => 'city_id Requerido',
                'direccionEmpresa.required' => 'direccionEmpresa Requerido',
                'representante.required' => 'representante Requerido',
                'anio.required' => 'anio Requerido',
                'empleados.required' => 'empleados Requerido',
                'sector.required' => 'sector Requerido',
                'actividadPrincipal.required' => 'actividadPrincipal Requerido',
                'actividadSecundaria.required' => 'actividadSecundaria Requerido',
                'promedioVentas.required' => 'promedioVentas Requerido',
                'totalActivos.required' => 'totalActivos Requerido',
                'operacionesInternacionales.required' => 'operacionesInternacionales Requerido',
                'tipoOperaciones.required' => 'tipoOperaciones Requerido',
                'tieneSucursales.required' => 'tieneSucursales Requerido',
            ]);

            $usuario->update($request->all());
            $usuario->save();
            DB::commit();
            $success = true;

        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => 'Actualizado',
                'obj' => $usuario = Empresas::findOrFail($request->id),
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    public function actualizarLogo(Request $request)
    {

        $usuario = Empresas::findOrFail($request->id);

        try {
            $error = null;
            DB::beginTransaction();

            $path = "";
            $filename = "";

            if ($request->uploadImg != null) {
                // Subimos el logo alservidor
                $path = "images/logoEmpresa/";
                $filename = $request->nit . '.' . $request->uploadImg->getClientOriginalExtension();
                move_uploaded_file($request->file('uploadImg'), $path . $filename);
                //
            }

            $usuario->logo = $path . $filename;
            $usuario->save();

            DB::commit();
            $success = true;
        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => 'Imagen actualizada',
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,

            );
        }//error


        return response()->json($response);
    }

    function desactivarEmpresa(Request $request)
    {

        $usuario = Empresas::findOrFail($request->id);

        $error = null;
        DB::beginTransaction();
        try {

            $usuario->activo = false;
            $usuario->save();
            DB::commit();
            $success = true;

        } catch (\Exception $e) {
            $success = false;
            $error = $e->getMessage();
            DB::rollback();
        }

        if ($success) {
            //success
            $response = array(
                'status' => 'success',
                'msg' => 'Desactivado',
            );
        } else {

            $response = array(
                'status' => 'Error',
                'msg' => $error,
            );
        }//error


        return response()->json($response);

    }

    public function empresas($id = null)
    {
        if ($id == null) {
            return view('pages.empresas');
        } else {
            $empresa = Empresas::findOrFail($id);
            return view('pages.perfil.empresa', compact('empresa'));
        }
    }

    public function dashboardEmpresa()
    {

        $empresas = DB::table('Empresas')->count();

        $servicios = Empresas::where('sector', '=', 'Servicios')->count();
        $comercio = Empresas::where('sector', '=', 'Comercio')->count();
        $industria = Empresas::where('sector', '=', 'Industria')->count();
        $agricultura = Empresas::where('sector', '=', 'Agricultura')->count();

        $response = array(
            'status' => 'Success',
            'empresas' => $empresas,
            'servicios' => $servicios,
            'comercio' => $comercio,
            'industria' => $industria,
            'agricultura' => $agricultura,
        );


        return response()->json($response);

    }


}
