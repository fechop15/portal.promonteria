<?php

namespace App\Http\Controllers;

use App\empresas;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $usuarios = User::all();
        $empresas = Empresas::all();

        return view('pages.dashboard', compact('usuarios', 'empresas'));
    }

    public function form()
    {
        return view('layouts.form');
    }

    //plataforma//
    public function empresas($id = null)
    {
        if ($id == null) {
            return view('pages.empresas');
        } else {
            //$empresa = Empresas::findOrFail($id);
            //return view('pages.perfil.empresa',compact('empresa'));
            return view('pages.perfil.empresa');
        }
    }

    public function usuarios($id = null)
    {

        if ($id == null) {
            $roles = array();
            $role = Role::get();
            foreach ($role as $rol) {

                if (Auth::user()->getRoleNames()[0] == 'Super Admin' && $rol->name == 'Super Admin') {
                    array_push($roles, $rol);
                }
                if ($rol->name != 'Super Admin') {
                    array_push($roles, $rol);
                }
            }
            return view('pages.usuarios', compact('roles'));
        } else {
            $usuario = User::findOrFail($id);
            return view('pages.perfil.usuario', compact('usuario'));

        }

    }

    public function perfil()
    {
        $usuario = Auth::user();
        return view('pages.perfil.usuario', compact('usuario'));
    }

    public function roles()
    {
        $permissions = Permission::get();
        return view('pages.roles', compact('permissions'));
    }

    public function permisos()
    {
        return view('pages.permisos');
    }
}
