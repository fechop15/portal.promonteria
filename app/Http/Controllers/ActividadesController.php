<?php

namespace App\Http\Controllers;

use App\Actividades;
use Illuminate\Http\Request;

class ActividadesController extends Controller
{
    //

    function getActividades()
    {
        $actividades = Actividades::get();

        $response = array(
            'status' => 'success',
            'msg' => $actividades,
        );

        return response()->json($response);
    }
}
