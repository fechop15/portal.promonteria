<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class empresas extends Model
{
    use Notifiable;

    protected $fillable = [
        'razonSocial', 'nit', 'telefono',
        'city_id', 'direccionEmpresa',
        'representante', 'anio', 'empleados', 'sector',
        'actividadPrincipal', 'actividadSecundaria', 'promedioVentas', 'totalActivos',
        'operacionesInternacionales', 'tipoOperaciones', 'tieneSucursales', 'sucursales',
        'comentarios', 'web', 'portafolio', 'logo',
    ];
}
