{{--
            <li {{  request()->route()->getName() === 'bodega' ||
                    request()->route()->getName() === 'sub-bodega' ||
                    request()->route()->getName() === 'producto'? ' class=active' : '' }}>
                <a data-toggle="collapse"
                   href="#pagesExamples" {{ request()->route()->getName() === 'bodega' ||
                                            request()->route()->getName() === 'sub-bodega' ||
                                            request()->route()->getName() === 'producto' ? ' aria-expanded=true' : 'aria-expand=false class=collapsed' }}>
                    <i class="material-icons">image</i>
                    <p>Inventario
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ request()->route()->getName() === 'bodega' ||
                                        request()->route()->getName() === 'sub-bodega' ||
                                        request()->route()->getName() === 'producto' ? 'in' : '' }}"
                     id="pagesExamples">
                    <ul class="nav">
                        <li {{ request()->route()->getName() === 'bodega' ? ' class=active' : '' }}>
                            <a href="{{ route('bodega') }}">
                                <span class="sidebar-mini">BO</span>
                                <span class="sidebar-normal">Bodegas</span>
                            </a>
                        </li>
                        <li {{ request()->route()->getName() === 'sub-bodega' ? ' class=active' : '' }}>
                            <a href="{{ route('sub-bodega') }}">
                                <span class="sidebar-mini">SB</span>
                                <span class="sidebar-normal">Sub Bodegas</span>
                            </a>
                        </li>

                        <li {{ request()->route()->getName() === 'producto' ? ' class=active' : '' }}>
                            <a href="{{ route('producto') }}">
                                <span class="sidebar-mini">PR</span>
                                <span class="sidebar-normal">Producto</span>
                            </a>
                        </li>


                    </ul>
                </div>
            </li>--}}


<div class="sidebar sidebar-style-2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="/images/perfil/{{auth()->user()->imagen}}" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									{{ auth()->user()->name }}
									<span class="user-level">
                                    {{ auth()->user()->getRoleNames()[0]}}
                                    </span>
									<span class="caret"></span>
								</span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="{{route('perfil')}}">
                                    <span class="link-collapse">Mi Cuenta</span>
                                </a>
                            </li>

                            <li>
                                <a href="#settings">
                                    <span class="link-collapse">Configuracion</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('logout') }}">
                                    <span class="link-collapse">Salir</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav nav-primary">

                <li class="nav-item {{ request()->route()->getName() === 'inicio' ? ' active' : '' }}">
                    <a href="{{ route('inicio') }}">
                        <i class="fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                @can('ver.empresas')
                <li class="nav-item {{ request()->route()->getName() === 'empresas' ? ' active' : '' }}">
                    <a href="{{ route('empresas') }}">
                        <i class="fas fa-home"></i>
                        <p>Empresas</p>
                    </a>
                </li>
                @endcan

                @can('ver.usuarios')
                    <li class="nav-item {{ request()->route()->getName() === 'usuarios' ? ' active' : '' }}">
                        <a href="{{ route('usuarios') }}">
                            <i class="fas fa-user"></i>
                            <p>Usuarios</p>
                        </a>
                    </li>
                @endcan

                @can('ver.roles')
                    <li class="nav-item {{ request()->route()->getName() === 'roles' ? ' active' : '' }}">
                        <a href="{{ route('roles') }}">
                            <i class="fas fa-user"></i>
                            <p>Roles</p>
                        </a>
                    </li>
                @endcan

                @role('Super Admin')
                <li class="nav-item {{ request()->route()->getName() === 'permisos' ? ' active' : '' }}">
                    <a href="{{ route('permisos') }}">
                        <i class="fas fa-user"></i>
                        <p>Permisos</p>
                    </a>
                </li>
                @endrole

            </ul>
        </div>
    </div>
</div>
