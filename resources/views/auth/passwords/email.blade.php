@extends('layouts.app')

@section('content')

    <div class="container container-login container-transparent animated fadeIn">
        <h3 class="text-center">{{ __('Restablecer contraseña') }}</h3>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" action="{{ route('password.email')}}">
            @csrf

            <div class="login-form">
                <div class="form-group">
                    <label for="email" class="placeholder"><b>{{ __('Correo') }}</b></label>

                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                           value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                 </span>
                    @enderror
                </div>

                <div class="row form-action">
                    <div class="col-md-6">
                        <a href="{{ route('login') }}" id="show-signin" class="btn btn-danger btn-link w-100 fw-bold">{{ __('Cancelar') }}</a>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-secondary w-100 fw-bold">
                            {{ __('Restablecer') }}
                        </button>
                    </div>
                </div>

            </div>
        </form>

    </div>

@endsection
