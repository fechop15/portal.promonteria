@extends('layouts.app')

@section('content')

    <div class="container container-signup container-transparent animated fadeIn">
        <h3 class="text-center">{{ __('Registrarse') }}</h3>
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="login-form">
                <div class="form-group">
                    <label for="name" class="placeholder"><b>{{ __('Nombre Completo') }}</b></label>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
             </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email" class="placeholder"><b>{{ __('Correo') }}</b></label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
            </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="passwordsignin" class="placeholder"><b>{{ __('Contraseña') }}</b></label>
                    <div class="position-relative">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        <div class="show-password">
                            <i class="icon-eye"></i>
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                </span>
                        @enderror

                    </div>
                </div>
                <div class="form-group">
                    <label for="confirmpassword" class="placeholder"><b>{{ __('Confirmar Contraseña') }}</b></label>
                    <div class="position-relative">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        <div class="show-password">
                            <i class="icon-eye"></i>
                        </div>
                    </div>
                </div>
                <div class="row form-sub m-0">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="agree" id="agree" required>
                        <label class="custom-control-label" for="agree">{{ __('Acepto los terminos y condiciones.') }}</label>
                    </div>
                </div>
                <div class="row form-action">
                    <div class="col-md-6">
                        <a href="{{ route('login') }}" id="show-signin" class="btn btn-danger btn-link w-100 fw-bold">{{ __('Cancelar') }}</a>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-secondary w-100 fw-bold">
                            {{ __('Registrarme') }}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
