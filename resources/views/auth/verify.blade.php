@extends('layouts.app')

@section('content')

    <div class="container container-login container-transparent animated fadeIn">
        <h3 class="text-center">{{ __('Verificar Tu Direccion de Correo') }}</h3>

        @if (session('resent'))
            <div class="alert alert-success" role="alert">
                {{ __('A fresh verification link has been sent to your email address.') }}
            </div>
        @endif

        {{ __('Antes de continuar, consulte su correo electrónico para ver un enlace de verificación.') }}
        {{ __('Si no ha recibido el correo electrónico.') }},
        <a href="{{ route('verification.resend') }}">{{ __('haga clic aquí para solicitar otra') }}</a>.

    </div>

@endsection
