@extends('layouts.dashboard')

@section('content')

    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Empresas</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Empresas</a>
                    </li>
                </ul>
            </div>
            <div class="row">

                <div class="col-md-12" id="card-empresas">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Empresas Registradas</h4>


                                <a href="#" class="btn btn-primary btn-round ml-auto"
                                   style="color:white; margin-right: 10px"
                                   id="nueva-empresa">
                                    <i class="fa fa-plus"></i>
                                    Nueva Empresa
                                </a>

                                <a href="{{route('form')}}" class="btn btn-primary btn-round "
                                   target="_blank"
                                   style="color:white">
                                    <i class="fa fa-plus"></i>
                                    Nueva Empresa (Link Externo)
                                </a>

                                <a href="#" class="btn btn-primary btn-round "
                                   style="color:white; margin-left: 10px"
                                   id="reloadTabla">
                                    <i class="fa fa-undo-alt"></i>
                                </a>

                            </div>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table id="tablaEmpleados" class="display table table-striped table-hover"
                                       style="width: 100%;">
                                    <thead>
                                    <tr>
                                        <th>Razon Social</th>
                                        <th>NIT</th>
                                        <th>Descripcion</th>
                                        <th>Creado</th>
                                        <th>Actualizado</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Razon Social</th>
                                        <th>NIT</th>
                                        <th>Descripcion</th>
                                        <th>Creado</th>
                                        <th>Actualizado</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 animated fadeIn" id="card-registro" style="display: none">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">
                                    <span class="fw-mediumbold" id="accion-modal">Registrar</span>
                                    <span class="fw-light">Empresa</span>
                                </h4></div>
                        </div>
                        <div class="card-body">

                            <form id="empresa-form">
                                @csrf
                                <input type="hidden" id="id" name="id" value="1">

                                <div class="row">
                                    <!--
                                    <div class="col-md-12">
                                        <h4 class="info-text">Tell us who you are.</h4>
                                    </div>
                                    -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Razon Social:</label>
                                            <input name="razonSocial" id="razonSocial" autocomplete="nio" type="text"
                                                   class="form-control" required="">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Nit :
                                                <span
                                                    style="font-weight: lighter">(Sin digito de verificación)</span>
                                            </label>
                                            <input name="nit" id="nit" autocomplete="nope" type="text"
                                                   maxlength="9"
                                                   class="form-control"
                                                   required="">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Telefono :</label>
                                            <input name="telefono" id="telefono" autocomplete="nope" type="text"
                                                   class="form-control" required="">
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Pais: </label>
                                            <div class="select2-input">
                                                <select name="pais" id="pais" class="form-control" required
                                                        style="width: 100%">
                                                    <option></option>
                                                    <option selected value="CO">Colombia</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Departamento:</label>
                                            <div class="select2-input">
                                                <select name="departamento" id="departamento" class="form-control"
                                                        style="width: 100%"
                                                        required>
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Ciudad:</label>
                                            <div class="select2-input">
                                                <select name="city_id" id="city_id" class="form-control" required
                                                        style="width: 100%">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Direccion:</label>
                                            <input name="direccionEmpresa" id="direccionEmpresa"
                                                   autocomplete="new-password"
                                                   type="text"
                                                   class="form-control" required="">
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Representante Legal:</label>
                                            <input name="representante" id="representante" autocomplete="new-password"
                                                   type="text"
                                                   class="form-control" required="">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Año de creación:</label>
                                            <input name="anio" id="anio" autocomplete="new-password" type="number"
                                                   class="form-control" required="">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Numero de empleados:</label>
                                            <input name="empleados" id="empleados" autocomplete="new-password"
                                                   type="number"
                                                   class="form-control" required="">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Sector de la empresa:</label>
                                            <div class="select2-input ">
                                                <select name="sector" id="sector" class="form-control width-100"
                                                        style="width: 100%"
                                                        required>
                                                    <option></option>
                                                    <option value="Servicios">Servicios</option>
                                                    <option value="Comercio">Comercio</option>
                                                    <option value="Turismo">Turismo</option>
                                                    <option value="Agroindustria">Agroindustria</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Actividad Economica Principal:</label>
                                            <div class="select2-input">
                                                <select name="actividadPrincipal" id="actividadPrincipal"
                                                        class="form-control"
                                                        required
                                                        style="width: 100%">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Actividad Economica Secundaria:</label>
                                            <div class="select2-input">
                                                <select name="actividadSecundaria" id="actividadSecundaria"
                                                        class="form-control"
                                                        style="width: 100%"
                                                        required>
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Promedio de ventas:
                                                <span style="font-weight: lighter">(en USD)</span>
                                            </label>
                                            <div class="select2-input">
                                                <select name="promedioVentas" id="promedioVentas"
                                                        class="form-control"
                                                        style="width: 100%"
                                                        required>
                                                    <option></option>
                                                    <option value="Menos de $500">Menos de $500</option>
                                                    <option value="Entre $1000 y $3000">Entre $1000 y $3000</option>
                                                    <option value="Entre $3000 y $6000">Entre $3000 y $6000</option>
                                                    <option value="Más de $6000">Más de $6000</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Total Activos:</label>
                                            <div class="select2-input">
                                                <select name="totalActivos" id="totalActivos" class="form-control"
                                                        style="width: 100%"
                                                        required>
                                                    <option></option>
                                                    <option value="Menos de $500">Menos de $500</option>
                                                    <option value="Entre $1000 y $3000">Entre $1000 y $3000</option>
                                                    <option value="Entre $3000 y $6000">Entre $3000 y $6000</option>
                                                    <option value="Más de $6000">Más de $6000</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Realiza Operaciones Internacionales?</label>
                                            <div class="select2-input">
                                                <select name="operacionesInternacionales"
                                                        id="operacionesInternacionales"
                                                        class="form-control"
                                                        style="width: 100%"
                                                        required>
                                                    <option></option>
                                                    <option value="S">Si</option>
                                                    <option value="N">No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tipo Operaciones:</label>
                                            <div class="select2-input">
                                                <select name="tipoOperaciones" class="form-control"
                                                        id="tipoOperaciones"
                                                        style="width: 100%"
                                                        required>
                                                    <option></option>
                                                    <option value="N">Nacionales</option>
                                                    <option value="I">Internacionales</option>
                                                    <option value="A">Ambas</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Sucursales en otras Ciudades?</label>
                                            <div class="select2-input">
                                                <select name="tieneSucursales" class="form-control"
                                                        id="tieneSucursales"
                                                        style="width: 100%"
                                                        required>
                                                    <option></option>
                                                    <option value="S">Si</option>
                                                    <option value="N">No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Ciudades:</label>
                                            <div class="select2-input">
                                                <input name="sucursales" id="sucursales" autocomplete="nio"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Comentarios adicionales: </label>

                                            <textarea name="comentarios" id="comentarios" rows="1"
                                                      class="form-control"
                                            ></textarea>
                                        </div>
                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Pagina Web:</label>
                                            <input name="web" id="web" autocomplete="nio" type="text"
                                                   class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Link Portafolio:</label>
                                            <input name="portafolio" id="portafolio" autocomplete="nio" type="text"
                                                   class="form-control">
                                        </div>
                                    </div>


                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Logo :</label>
                                            <div class="input-file input-file-image">
                                                <img class="img-upload-preview img-circle" width="100"
                                                     id="logoEmpresa"
                                                     height="100"
                                                     src="https://placehold.it/100x100" alt="preview">
                                                <input type="file" class="form-control form-control-file"
                                                       id="uploadImg" name="uploadImg" accept="image/*"
                                                       onchange="subirLogo()"
                                                >
                                                <label for="uploadImg"
                                                       class=" label-input-file btn btn-primary">Subir
                                                    Imagen</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </form>
                        </div>

                        <div class="card-footer text-right">
                            <button type="button" onclick="actualizarEmpresa()" class="btn btn-primary" style="">
                                Actualizar
                            </button>
                            <button type="button" id="actualizar" class="btn btn-primary" style="display: none;">
                                Actualizar
                            </button>
                            <button type="button" id="eliminar" class="btn btn-warning" style="display: none;">
                                Eliminar
                            </button>
                            <button type="button" id="cancelar-registro" class="btn btn-danger">Cancelar</button>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>

    @include('pages.modal.permisos')

@endsection

@section('scripts')
    <!--
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    -->
    <script type="text/javascript">


        var IDUSUARIO = 0;

        $('#card-registro').hide()

        var TABLA = $('#tablaEmpleados').DataTable({

            /*
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    text: 'Excel',
                    className: 'btn btn-primary exportExcel',
                    filename: 'Empresas ProMonteria',
                    exportOptions: {
                        columns: ':visible'
                    }
                }
            ],
            */

            "ajax": {
                "url": "{{ url('recurso/empresas') }}",
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            id: data.msg[item].id,
                            anio: data.msg[item].anio,
                            actividadPrincipal: data.msg[item].actividadPrincipal,
                            actividadSecundaria: data.msg[item].actividadSecundaria,
                            city_id: data.msg[item].city_id,
                            comentarios: data.msg[item].comentarios,
                            created_at: data.msg[item].created_at,
                            direccionEmpresa: data.msg[item].direccionEmpresa,
                            empleados: data.msg[item].empleados,
                            nit: data.msg[item].nit,
                            operacionesInternacionales: data.msg[item].operacionesInternacionales,
                            portafolio: data.msg[item].portafolio,
                            promedioVentas: data.msg[item].promedioVentas,
                            razonSocial: data.msg[item].razonSocial,
                            representante: data.msg[item].representante,
                            sector: data.msg[item].sector,
                            sucursales: data.msg[item].sucursales,
                            telefono: data.msg[item].telefono,
                            tieneSucursales: data.msg[item].tieneSucursales,
                            tipoOperaciones: data.msg[item].tipoOperaciones,
                            totalActivos: data.msg[item].totalActivos,
                            updated_at: data.msg[item].updated_at,
                            logo: data.msg[item].logo,
                            web: data.msg[item].web,
                            opciones: opciones(data.msg[item].id)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "razonSocial"},
                {data: "nit"},
                {data: "actividadPrincipal"},
                {data: "created_at"},
                {data: "updated_at"},
                {data: "opciones"},
            ],
        });

        function opciones(id) {
            var opciones = '';
            @can('editar.empresas')
                opciones += '<button type="button" class="btn btn-primary btn-xs editar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-pen"></i>\n' +
                ' </button>';
            @endcan
                opciones += '' +
                '<button type="button" class="btn btn-success btn-xs ver" ' +
                '           data-toggle="tooltip" data-placement="top" title="Ver Perfil" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-eye"></i>\n' +
                ' </button>';
            @can('eliminar.empresas')

                opciones += '' +
                '<button type="button" class="btn btn-danger btn-xs eliminar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Edit">' +
                '           <i class="fas fa-trash"></i>\n' +
                ' </button>';
            @endcan
                return opciones;
        }

        $("#modalGuardar").on('click', function () {
            $('#modal').modal('show');
            $('#permisos-form')[0].reset();
            $('#actualizar').hide();
            $('#eliminar').hide();
            $('#guardar').show();
            $("#error").hide();
            $("#accion-modal").html('Registrar');

        });

        TABLA.on('click', '.codigo', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            $("#modal-cortesia").modal('show');
            $('#form-cortesia')[0].reset();
            $("#id").val(data.Id);
        });

        $("#guardar-cortesia").on('click', function () {
            $("#error-cortesia").hide();
            $('#modal-cortesia .modal-content').addClass("is-loading");
            $("#guardar-cortesia").prop('disabled', true);
            $.ajax({
                url: '/crear-cortesia',
                type: 'POST',
                data: {
                    usuario: $('#usuario-cortesia').val(),
                    codigo: $('#codigo-cortesia').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                $('#form-cortesia')[0].reset();
                $('#modal-cortesia').modal('hide');
                $("#guardar-cortesia").prop('disabled', false);

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.msg,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error-cortesia").html(value[0]);
                    $("#error-cortesia").show();
                });


            }).always(function () {
                $('#modal-cortesia .modal-content').removeClass("is-loading");
                $("#guardar-cortesia").prop('disabled', false);
            });

        });

        TABLA.on('click', '.editar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();

            getStateofCity(data.city_id)

            $('#card-empresas').hide()
            $('#card-registro').show()
            $('#card-registro').addClass('animated fadeIn');

            $('#empresa-form')[0].reset();
            $('#actualizar').hide();
            $('#eliminar').hide();
            $('#guardar').show();
            $("#error").hide();
            $("#accion-modal").html('Editar');

            $("#id").val(data.id);
            $("#razonSocial").val(data.razonSocial);
            $("#nit").val(data.nit);
            $("#telefono").val(data.telefono);
            $("#pais").val('CO').trigger('change');
            $("#direccionEmpresa").val(data.direccionEmpresa);
            $("#representante").val(data.representante);
            $("#anio").val(parseInt(data.anio));
            $("#empleados").val(data.empleados);
            $("#sector").val(data.sector).trigger('change');
            $("#actividadPrincipal").val(data.actividadPrincipal).trigger('change');
            $("#actividadSecundaria").val(data.actividadSecundaria).trigger('change');
            $("#promedioVentas").val(data.promedioVentas).trigger('change');
            $("#totalActivos").val(data.totalActivos).trigger('change');
            $("#operacionesInternacionales").val(data.operacionesInternacionales).trigger('change');
            $("#tipoOperaciones").val(data.tipoOperaciones).trigger('change');
            $("#tieneSucursales").val(data.tieneSucursales).trigger('change');
            $("#sucursales").val(data.sucursales);
            $("#comentarios").val(data.comentarios);
            $("#web").val(data.web);
            $("#portafolio").val(data.portafolio);

            if (data.logo != null || data.logo != "") {
                $('#logoEmpresa').attr('src', '../' + data.logo);
            } else {
                $('#logoEmpresa').attr('src', 'https://placehold.it/100x100');
            }

        });

        TABLA.on('click', '.eliminar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();

            console.log(data.id)

            swal({
                title: 'Estas seguro?',
                text: "No podras revertir este cambio!",
                type: 'warning',
                buttons: {
                    confirm: {
                        text: 'Si, eliminar',
                        className: 'btn btn-success'
                    },
                    cancel: {
                        visible: true,
                        text: 'No, cancelar',
                        className: 'btn btn-danger'
                    }
                }
            }).then((willDelete) => {
                if (willDelete) {

                    let csrfToken = '{{ csrf_token() }}';

                    $.ajax({
                        url: '/desactivarEmpresa',
                        type: 'POST',
                        headers: {
                            'X-CSRF-Token': csrfToken
                        },
                        data: {
                            id: data.id
                        }
                    }).done(function (response) {


                        if (response.status !== 'Error') {

                            reloadTabla();

                            swal("La empresa ha sido eliminada", {
                                buttons: {
                                    confirm: {
                                        className: 'btn btn-success'
                                    }
                                }
                            });

                        }

                    }).fail(function (error) {
                        console.log(error);

                        let obj = error.responseJSON.errors;
                        $.each(obj, function (key, value) {

                            swal("Error!", value[0], {
                                icon: "error",
                                buttons: {
                                    confirm: {
                                        className: 'btn btn-danger'
                                    }
                                },
                            });

                        });

                    });


                } else {
                    swal("Your imaginary file is safe!", {
                        buttons: {
                            confirm: {
                                className: 'btn btn-success'
                            }
                        }
                    });
                }
            });

        });

        $("#guardar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#guardar").prop('disabled', true);
            $.ajax({
                url: "{{ url('recurso/permissions') }}",
                type: 'POST',
                data: {
                    name: $('#nombre').val(),
                    description: $('#descripcion').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                $('#permisos-form')[0].reset();
                $('#modal').modal('hide');
                $("#guardar").prop('disabled', false);
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#guardar").prop('disabled', false);
            });

        });

        $("#actualizar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#actualizar").prop('disabled', true);
            $.ajax({
                url: "{{ url('recurso/permissions/') }}/" + $('#id').val(),
                type: 'PUT',
                data: {
                    name: $('#nombre').val(),
                    description: $('#descripcion').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                $('#permisos-form')[0].reset();
                $('#modal').modal('hide');
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });

        $("#eliminar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#eliminar").prop('disabled', true);
            $.ajax({
                url: "{{ url('recurso/permissions/') }}/" + $('#id').val(),
                type: 'DELETE',
            }).done(function (response) {
                $('#permisos-form')[0].reset();
                $('#modal').modal('hide');
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });

        $("#nueva-empresa").on('click', function () {


            $('#card-empresas').hide()
            $('#card-registro').show()
            $('#card-registro').addClass('animated fadeIn');

            $('#empresa-form')[0].reset();
            $('#actualizar').hide();
            $('#eliminar').hide();
            $('#guardar').show();
            $("#error").hide();
            $("#accion-modal").html('Registrar');

        });

        $("#reloadTabla").on('click', function () {

            reloadTabla();

        });

        $("#cancelar-registro").on('click', function () {

            $('#card-registro').hide();
            $('#card-empresas').show();
            $('#card-empresas').addClass('animated fadeIn');

            $('#empresa-form')[0].reset();

        });

        function subirLogo() {

            let formData = new FormData(document.getElementById("empresa-form"));

            let csrfToken = '{{ csrf_token() }}';

            $.ajax({
                url: '/actualizarLogo',
                type: 'POST',
                headers: {
                    'X-CSRF-Token': csrfToken
                },
                data: formData,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {

                console.log(response);

                if (response.status !== 'Error') {

                    $.notify({
                        icon: 'flaticon-alarm-1',
                        title: 'Actualizacion',
                        message: 'El logo de la empresa se ha actualizado correctamente',
                    }, {
                        type: 'info',
                        placement: {
                            from: "bottom",
                            align: "right"
                        },
                        time: 1000,
                    });

                }

            }).fail(function (error) {
                console.log(error);

                let obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {

                    swal("Error!", value[0], {
                        icon: "error",
                        buttons: {
                            confirm: {
                                className: 'btn btn-danger'
                            }
                        },
                    });

                });

            });

        }

        function reloadTabla() {
            TABLA.clear().draw();
            TABLA.ajax.reload();
        }

        function getStateofCity(id) {

            $.ajax({
                url: "/getStatesOfCity",
                data: {
                    city: id
                },
                async: false,

                success: function (departameto) {
                    getCountrieOfState(departameto, id);
                }
            });

        }

        function getCountrieOfState(departamento, ciudad) {

            console.log(departamento.msg[0].state_id)
            console.log(ciudad)

            $('.card .card-body').addClass("is-loading");

            $("#select-pais").val('CO').trigger('change');

            var cities = [];


            $.get(
                "/getCities", {'state': departamento.msg[0].state_id}
            ).done(function (data) {


                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }

                $('#city_id').empty().trigger("change");

                $("#city_id").select2({
                    allowClear: true,
                    theme: "bootstrap",
                    placeholder: "Selecciona una ciudad...",
                    data: cities
                });

                $("#departamento").val(departamento.msg[0].state_id).trigger('change')

                $("#city_id").val(ciudad).trigger('change')

                $('.card .card-body').removeClass("is-loading");

            });


        }

        function cargarDepartamentos() {

            let STATES = [];

            $.ajax({
                url: "/getStates",
                async: false
            }).done(function (data) {

                console.log(data)

                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    STATES.push(itemSelect2)
                }
                $('#departamento').select2({
                    theme: "bootstrap",
                    placeholder: "Selecciona un departamento...",
                    allowClear: true,
                    data: STATES
                });

            });

        }

        function cargarMunicipios(codDepartamento) {


            var cities = [];
            $("#city_id").html('').select2();
            $.get(
                "/getCities", {'state': codDepartamento}
            ).done(function (data) {
                for (var item in data.msg) {
                    var itemSelect2 = {
                        id: data.msg[item].id,
                        text: data.msg[item].name
                    };
                    cities.push(itemSelect2)
                }
                $('#city_id').select2({
                    allowClear: true,
                    theme: "bootstrap",
                    placeholder: "Selecciona una ciudad...",
                    data: cities
                });


            });

        }

        function cargarActividades() {

            let actividades = [];

            $.get(
                "/getActividades",
            ).done(function (data) {

                for (let item in data.msg) {
                    let itemSelect2 = {
                        id: data.msg[item].codigo,
                        text: data.msg[item].descripcion
                    };
                    actividades.push(itemSelect2)
                }
                $('#actividadPrincipal').select2({
                    theme: "bootstrap",
                    placeholder: "Selecciona una Actividad...",
                    allowClear: true,
                    data: actividades
                });

                $('#actividadSecundaria').select2({
                    theme: "bootstrap",
                    placeholder: "Selecciona una Actividad...",
                    allowClear: true,
                    data: actividades
                });

            });

        }

        function actualizarEmpresa() {

            let formData = new FormData(document.getElementById("empresa-form"));

            console.log('printing data...');
            console.log($("#id").val())
            let csrfToken = '{{ csrf_token() }}';
            console.log(csrfToken)

            $.ajax({
                url: '/actualizarEmpresa',
                type: 'POST',
                data: $("#empresa-form").serialize(),

            }).done(function (response) {

                console.log(response);

                reloadTabla();

                if (response.status !== 'Error') {

                    $.notify({
                        icon: 'flaticon-alarm-1',
                        title: 'Actualizacion',
                        message: 'Los datos se han actualizado con exito',
                    }, {
                        type: 'info',
                        placement: {
                            from: "bottom",
                            align: "right"
                        },
                        time: 1000,
                    });

                    $('#card-empresas').show()
                    $('#card-registro').hide()
                    $('#card-empresas').addClass('animated fadeIn');
                    $('#empresa-form')[0].reset();

                    $('#card').css("display", "");
                    $('#formWizzard').css("display", "none");

                    $('#titleEmpresa').text(response.msg.razonSocial);

                }

            }).fail(function (error) {
                console.log(error);

                let obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {

                    swal("Error!", value[0], {
                        icon: "error",
                        buttons: {
                            confirm: {
                                className: 'btn btn-danger'
                            }
                        },
                    });

                });

            });


        }

        $(document).ready(function () {

            cargarDepartamentos();
            cargarActividades();

            $('#departamento').on('change', function (e) {
                cargarMunicipios($("#departamento").val());
            });

            $(".select2-container").removeAttr("style");

            $('#pais').select2({
                theme: "bootstrap",
                placeholder: "Selecciona un pais...",
                allowClear: true
            });


            $('#city_id').select2({
                theme: "bootstrap",
                placeholder: "Selecciona una ciudad...",
                allowClear: true
            });

            $('#sector').select2({
                theme: "bootstrap",
                placeholder: "Selecciona un sector...",
                allowClear: true
            });

            $('#actividadPrincipal').select2({
                theme: "bootstrap",
                placeholder: "Selecciona una actividad...",
                allowClear: true
            });

            $('#actividadSecundaria').select2({
                theme: "bootstrap",
                placeholder: "Selecciona una actividad...",
                allowClear: true
            });

            $('#promedioVentas').select2({
                theme: "bootstrap",
                placeholder: "Selecciona un promedio...",
                allowClear: true
            });

            $('#totalActivos').select2({
                theme: "bootstrap",
                placeholder: "Selecciona el total de activos...",
                allowClear: true
            });

            $('#operacionesInternacionales').select2({
                theme: "bootstrap",
                placeholder: "Selecciona una opcion...",
                allowClear: true
            });

            $('#tipoOperaciones').select2({
                theme: "bootstrap",
                placeholder: "Selecciona un tipo...",
                allowClear: true
            });

            $('#tieneSucursales').select2({
                theme: "bootstrap",
                placeholder: "Selecciona una opcion...",
                allowClear: true
            });

        });

    </script>
@endsection
