<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-primary">
                <h5 class="modal-title">
                    <span class="fw-mediumbold" id="accion-modal">Nuevo</span>
                    <span class="fw-light">Rol</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-rol">
                    @csrf
                    <input type="hidden" id="id">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name" class="text-right">
                                    Nombre
                                    <span class="required-label">*</span>
                                </label>
                                <input type="text" class="form-control" id="nombre" name="name"
                                       placeholder="Nombre del rol" required="">
                            </div>
                        </div>
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-12">
                            <h3>Lista de permisos</h3>
                            <div class="form-group">

                            <div class="row">

                                @foreach($permissions as $permission)
                                    <div class="col-sm-6">

                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="permissions[]" class="custom-control-input"
                                                   value="{{$permission->id}}" id="permiso{{$permission->id}}">
                                            <label class="custom-control-label"
                                                   for="permiso{{$permission->id}}"> {{ $permission->name }}</label>
                                            <em>({{ $permission->description }})</em>
                                        </div>

                                    </div>
                                @endforeach

                            </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="alert alert-danger text-danger" id="error" style="display: none">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
                <button type="button" id="actualizar" class="btn btn-primary">Actualizar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
