@extends('layouts.dashboard')

@section('content')

    <div class="content">
        <div class="page-inner">
            <h4 class="page-title">Perfil de {{$empresa->razonSocial}}</h4>
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-with-nav">
                        <div class="card-header">
                            <div class="row row-nav-line">
                                <ul class="nav nav-tabs nav-line nav-color-secondary" role="tablist">
                                    <li class="nav-item submenu"><a class="nav-link active show" data-toggle="tab"
                                                                    href="#home" role="tab"
                                                                    aria-selected="true">Empresa</a>
                                    </li>
                                    <li class="nav-item submenu"><a class="nav-link" data-toggle="tab" href="#profile"
                                                                    role="tab" aria-selected="false">Actividades</a>
                                    </li>
                                    <li class="nav-item submenu"><a class="nav-link" data-toggle="tab" href="#settings"
                                                                    role="tab" aria-selected="false">Web</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row mt-3">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Razon Social</label>
                                        <input type="text" class="form-control" name="name" placeholder="Razon Social"
                                               value="{{$empresa->razonSocial}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Nit</label>
                                        <input type="text" class="form-control" name="nit" placeholder="nit"
                                               maxlength="9"
                                               value="{{$empresa->nit}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Telefono</label>
                                        <input type="text" class="form-control" id="telefono" name="telefono"
                                               value="{{$empresa->telefono}}" placeholder="Telefono">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Pais</label>
                                        <select class="form-control" name="pais">
                                            <option selected value="CO">Colombia</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Departamento</label>
                                        <select class="form-control" name="departamento">
                                            <option selected value="COR">Cordoba</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Ciudad</label>
                                        <select class="form-control" name="pais">
                                            <option selected value="MON">Monteria</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Direccion</label>
                                        <input type="text" class="form-control" id="telefono" name="direccion"
                                               value="{{$empresa->direccion}}" placeholder="Direccion">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Representante Legal</label>
                                        <input type="text" class="form-control" id="representante" name="representante"
                                               value="{{$empresa->representante}}" placeholder="Representante Legal">
                                    </div>
                                </div>

                            </div>
                            <div class="row mt-3">

                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Año de creacion</label>
                                        <input type="text" class="form-control" id="representante" name="representante"
                                               value="{{$empresa->anio}}" placeholder="Año de creacion">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Año de creacion</label>
                                        <input type="text" class="form-control" id="representante" name="representante"
                                               value="{{$empresa->empleados}}" placeholder="Año de creacion">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Sector</label>
                                        <select class="form-control" name="sector" id="sector">
                                            <option value="Servicios">Servicios</option>
                                            <option value="Comercio">Comercio</option>
                                            <option value="Turismo">Turismo</option>
                                            <option value="Agroindustria">Agroindustria</option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="text-right mt-3 mb-3">
                                <button class="btn btn-success">Guardar</button>
                                <button class="btn btn-danger">Borrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-header"
                             style="background-image: url('');border-bottom: 0px solid #ebecec !important;">
                            <div class="profile-picture">
                                <div class="avatar avatar-xxl" style="width: 18rem;">
                                    <img src=" ../{{$empresa->logo}}" alt="..."
                                         class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="user-profile text-center">
                                <div class="name">{{$empresa->razonSocial}}</div>
                                <div class="job">Nit: {{$empresa->nit}}</div>
                                <div class="desc">{{$empresa->representante}}</div>

                                <div class="view-profile">
                                    <a href="{{$empresa->web}}" class="btn btn-secondary btn-block">Visitar Web</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row user-stats text-center">
                                <div class="col">
                                    <div class="number">{{$empresa->sector}}</div>
                                    <div class="title">Sector</div>
                                </div>
                                <div class="col">
                                    <div class="number">{{$empresa->empleados}}</div>
                                    <div class="title">Empleados</div>
                                </div>
                                <div class="col">
                                    <div class="number">{{$empresa->telefono}}</div>
                                    <div class="title">Telefono</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')



    <script type="text/javascript">

        let empresa = {!! json_encode($empresa)!!}
        console.log(empresa)

    </script>
@endsection
