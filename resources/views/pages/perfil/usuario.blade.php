@extends('layouts.dashboard')

@section('content')
    <div class="content">
        <div class="page-inner">
            <h4 class="page-title">Perfil: {{$usuario->name}}</h4>
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-with-nav">
                        <div class="card-header">
                            <div class="row row-nav-line">
                                <ul class="nav nav-tabs nav-line nav-color-secondary" role="tabpanel">
                                    <li class="nav-item">
                                        <a class="nav-link active show" data-toggle="tab" href="#home-tab"
                                           role="tab" aria-selected="true">Informacion</a>
                                    </li>
                                    @if(auth()->user()->id==$usuario->id)
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#profile-tab" role="tab"
                                               aria-selected="false">Contraseña</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#image-tab" role="tab"
                                               aria-selected="false">Imagen</a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>

                        <div class="card-body">

                            <div class="tab-content mt-2 mb-3" id="pills-tabContent">

                                <div class="tab-pane fade show active" id="home-tab" role="tabpanel"
                                     aria-labelledby="home-tab">
                                    <h2>Informacion de perfil</h2>
                                </div>
                                @if(auth()->user()->id==$usuario->id)

                                <div class="tab-pane fade" id="profile-tab" role="tabpanel"
                                     aria-labelledby="profile-tab">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h6 class="card-header-text">Cambio de Contraseña:</h6>

                                            <form id="form-password">

                                                <div class="form-group">
                                                    <label class="col-form-label">Contraseña Actual:</label>
                                                    <input type="text" class="form-control" id="passOld">

                                                    <label class="col-form-label">Contraseña nueva:</label>
                                                    <input type="text" class="form-control" id="pass">

                                                    <label class="col-form-label">Contraseña nueva
                                                        (Confirmacion):</label>
                                                    <input type="text" class="form-control" id="pass2">
                                                </div>

                                                <div class="form-group">
                                                    <div class="alert alert-danger" id="Perror"
                                                         style="display: none">
                                                    </div>
                                                </div>

                                            </form>
                                            <button type="button" class="btn btn-primary waves-effect waves-light"
                                                    onclick="cambiarPass()">Confirmar
                                            </button>

                                            <!-- end of project table -->
                                        </div>
                                        <div class="col-sm-6"></div>
                                        <!-- end of col-lg-12 -->
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="image-tab" role="tabpanel"
                                     aria-labelledby="image-tab">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h6 class="card-header-text">Cambio de Imagen:</h6>
                                                <div class="input-file input-file-image">
                                                    <img class="img-upload-preview img-circle" width="200" height="200" src="/images/perfil/{{$usuario->imagen}}" alt="preview">
                                                    <input type="file" class="form-control form-control-file" id="uploadImg1" name="avatar" accept="image/*" required="">
                                                    <label for="uploadImg1" class="  label-input-file btn btn-default btn-round">
													<span class="btn-label">
														<i class="fa fa-file-image"></i>
													</span>
                                                        Cambiar Imagen
                                                    </label>
                                                </div>
                                            <div class="form-group">
                                                <div class="alert alert-danger" id="error"
                                                     style="display: none">
                                                </div>
                                            </div>
                                            <!-- end of project table -->
                                        </div>
                                        <div class="col-sm-6"></div>
                                        <!-- end of col-lg-12 -->
                                    </div>
                                </div>

                                @endif

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-header" style="background-image: url('/assets/img/blogpost.jpg')">
                            <div class="profile-picture">
                                <div class="avatar avatar-xl">
                                    <img id="perfil" src="/images/perfil/{{$usuario->imagen}}" alt="..."
                                         class="avatar-img rounded-circle">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="user-profile text-center">
                                <div class="name">{{$usuario->name}}</div>
                                <div class="job">{{$usuario->email}}</div>
                                <div class="desc">{{ $usuario->getRoleNames()[0]}}</div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row user-stats text-center">
                                <div class="col">
                                    <div class="number">{{$usuario->created_at}}</div>
                                    <div class="title">Fecha Registro.</div>
                                </div>
                                <div class="col">
                                    <div class="number">{{$usuario->telefono}}</div>
                                    <div class="title">Telefono</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script type="text/javascript">

        function cambiarPass() {
            if ($("#pass").val() != $("#pass2").val()) {
                $("#Perror").html("Las Contraseña No Son Iguales");
                $("#Perror").show();
                return;
            }
            $("#Perror").hide();

            $.post(
                "/recurso/cambiar-pass", {
                    pass: $("#pass").val(),
                    passOld: $("#passOld").val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                }
            ).done(function (data) {
                console.log(data);
                if (data.status == "Error") {
                    $("#Perror").html(data.msg);
                    $("#Perror").show();
                } else {

                    $.notify({
                        icon: 'flaticon-success',
                        title: 'Felicidades',
                        message: 'Contraseña Actualizada con exito',
                    }, {
                        type: 'success',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        time: 1000,
                    });

                    $("#form-password")[0].reset();
                    $("#Perror").hide();
                }

            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                Object.entries(obj).forEach(([key, value]) => {
                    $("#Perror").html(value[0]);
                    $("#Perror").show();
                });

            });
        }

        $('#uploadImg1').change(function() {
            var data = new FormData();
            jQuery.each(jQuery('#uploadImg1')[0].files, function(i, file) {
                data.append('avatar', file);
            });
            data.append('_token', $('meta[name="csrf-token"]').attr('content'));
            $.ajax({
                url: '/recurso/cambiar-avatar',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false
            }).done(function (response) {
                console.log(response);
                if (response.status == "Error") {
                    $("#error").html(response.msg);
                    $("#error").show();
                } else {
                    $("#error").hide();
                    $("#perfil").attr("src",'/images/perfil/'+response.imagen);
                    $.notify({
                        icon: 'flaticon-success',
                        title: 'Felicidades',
                        message: response.msg,
                    }, {
                        type: 'success',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        time: 1000,
                    });
                }
                //return response;
            }).fail(function (error) {

                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });

            });
        });

    </script>

@endsection
