@extends('layouts.dashboard')

@section('content')

    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Permisos</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Permisos</a>
                    </li>
                </ul>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Permisos del sistema</h4>
                                <button class="btn btn-primary btn-round ml-auto" id="modalGuardar">
                                    <i class="fa fa-plus"></i>
                                    Nuevo Permiso
                                </button>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table id="tablaEmpleados" class="display table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Descripcion</th>
                                        <th>Creado</th>
                                        <th>Actualizado</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Descripcion</th>
                                        <th>Creado</th>
                                        <th>Actualizado</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.modal.permisos')

@endsection

@section('scripts')
    <script type="text/javascript">

        var IDUSUARIO=0;

        var TABLA = $('#tablaEmpleados').DataTable({
            "ajax": {
                "url": "{{ url('recurso/permissions') }}",
                "type": "GET",
                "dataSrc": function (data) {
                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            Id: data.msg[item].id,
                            Nombre: data.msg[item].name,
                            Descripcion: data.msg[item].description,
                            Tipo: data.msg[item].guard_name,
                            Creado: data.msg[item].created_at,
                            Actualizado: data.msg[item].updated_at,
                            Opciones: opciones(data.msg[item].id)
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "Nombre"},
                {data: "Tipo"},
                {data: "Descripcion"},
                {data: "Creado"},
                {data: "Actualizado"},
                {data: "Opciones"},
            ],
        });

        function opciones(id) {
            var opciones = '';

            opciones += '' +
                '<button type="button" class="btn btn-primary btn-xs editar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Editar" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-pen"></i>\n' +
                ' </button>';

            opciones += '' +
                '<button type="button" class="btn btn-danger btn-xs eliminar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Edit">' +
                '           <i class="fas fa-trash"></i>\n' +
                ' </button>';

            return opciones;
        }

        $("#modalGuardar").on('click', function () {
            $('#modal').modal('show');
            $('#permisos-form')[0].reset();
            $('#actualizar').hide();
            $('#eliminar').hide();
            $('#guardar').show();
            $("#error").hide();
            $("#accion-modal").html('Registrar');

        });

        TABLA.on('click', '.codigo', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            $("#modal-cortesia").modal('show');
            $('#form-cortesia')[0].reset();
            $("#id").val(data.Id);
        });

        $("#guardar-cortesia").on('click', function () {
            $("#error-cortesia").hide();
            $('#modal-cortesia .modal-content').addClass("is-loading");
            $("#guardar-cortesia").prop('disabled', true);
            $.ajax({
                url: '/crear-cortesia',
                type: 'POST',
                data: {
                    usuario: $('#usuario-cortesia').val(),
                    codigo: $('#codigo-cortesia').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                $('#form-cortesia')[0].reset();
                $('#modal-cortesia').modal('hide');
                $("#guardar-cortesia").prop('disabled', false);

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.msg,
                },{
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error-cortesia").html(value[0]);
                    $("#error-cortesia").show();
                });


            }).always(function () {
                $('#modal-cortesia .modal-content').removeClass("is-loading");
                $("#guardar-cortesia").prop('disabled', false);
            });

        });

        TABLA.on('click', '.editar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            $("#modal").modal('show');
            $('#permisos-form')[0].reset();
            $('#actualizar').show();
            $('#eliminar').hide();
            $('#guardar').hide();
            $("#error").hide();
            $("#accion-modal").html('Editar');
            $("#id").val(data.Id);
            $('#nombre').val(data.Nombre);
            $('#descripcion').val(data.Descripcion);
        });

        TABLA.on('click', '.eliminar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            $("#modal").modal('show');
            $('#permisos-form')[0].reset();
            $('#actualizar').hide();
            $('#eliminar').show();
            $('#guardar').hide();
            $("#error").hide();
            $("#accion-modal").html('Eliminar');
            $("#id").val(data.Id);
            $('#nombre').val(data.Nombre);
            $('#descripcion').val(data.Descripcion);
        });

        $("#guardar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#guardar").prop('disabled', true);
            $.ajax({
                url: "{{ url('recurso/permissions') }}",
                type: 'POST',
                data: {
                    name: $('#nombre').val(),
                    description: $('#descripcion').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                $('#permisos-form')[0].reset();
                $('#modal').modal('hide');
                $("#guardar").prop('disabled', false);
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                },{
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#guardar").prop('disabled', false);
            });

        });

        $("#actualizar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#actualizar").prop('disabled', true);
            $.ajax({
                url:  "{{ url('recurso/permissions/') }}/"+$('#id').val(),
                type: 'PUT',
                data: {
                    name: $('#nombre').val(),
                    description: $('#descripcion').val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                },

            }).done(function (response) {
                $('#permisos-form')[0].reset();
                $('#modal').modal('hide');
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                },{
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });

        $("#eliminar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#eliminar").prop('disabled', true);
            $.ajax({
                url:  "{{ url('recurso/permissions/') }}/"+$('#id').val(),
                type: 'DELETE',
            }).done(function (response) {
                $('#permisos-form')[0].reset();
                $('#modal').modal('hide');
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                },{
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });

    </script>
@endsection
