@extends('layouts.dashboard')

@section('content')

    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Usuarios</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Usuarios</a>
                    </li>
                </ul>
            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Usuarios del sistema</h4>
                                @can('crear.usuarios')
                                <button class="btn btn-primary btn-round ml-auto" id="modalGuardar">
                                    <i class="fa fa-plus"></i>
                                    Nuevo Usuario
                                </button>
                                @endcan
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table id="tablaUsuarios" class="display table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Correo</th>
                                        <th>Rol</th>
                                        <th>Registro</th>
                                        <th>Estado</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Correo</th>
                                        <th>Rol</th>
                                        <th>Registro</th>
                                        <th>Estado</th>
                                        <th class="text-right">Opciones</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('pages.modal.user')

@endsection

@section('scripts')
    <script type="text/javascript">

        $('#tablaUsuarios thead th').each(function () {
            var title = $(this).text();
            $(this).html(title + ' <input type="text" class="form-control" style="height: auto !important;" placeholder="Buscar ' + title + '" />');
        });
        var TABLA = $('#tablaUsuarios').DataTable({
            ajax: {
                url: "{{ url('recurso/users') }}",
                "dataSrc": function (data) {
                    var json = [];
                    console.log(data);
                    for (var item in data.msg) {
                        var itemJson = {
                            id: data.msg[item].id,
                            nombre: data.msg[item].nombre,
                            correo: data.msg[item].correo,
                            rol: data.msg[item].rol,
                            fecha_registro: data.msg[item].fecha_registro,
                            estado: data.msg[item].estado,
                            Opciones: opciones()
                        };
                        json.push(itemJson)
                    }
                    return json;
                }
            },
            columns: [
                {data: "nombre"},
                {data: "correo"},
                {data: "rol[0].name"},
                {data: "fecha_registro"},
                {data: "estado"},
                {data: "Opciones"},
            ],
        });

        TABLA.columns().every(function () {
            var table = this;
            $('input', this.header()).on('keyup change', function () {
                if (table.search() !== this.value) {
                    table.search(this.value).draw();
                }
            });
        });

        function opciones() {
            var opciones = '';
            @can('editar.usuarios')
                opciones += '<button type="button" class="btn btn-primary btn-xs editar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Actualizar" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-pen"></i>\n' +
                ' </button>';
            @endcan
                opciones += '' +
                '<button type="button" class="btn btn-success btn-xs ver" ' +
                '           data-toggle="tooltip" data-placement="top" title="Ver Perfil" data-original-title="Edit"' +
                '           style="margin-right: 5px;">\n' +
                '           <i class="fas fa-eye"></i>\n' +
                ' </button>';
            @can('eliminar.usuarios')

                opciones += '' +
                '<button type="button" class="btn btn-danger btn-xs eliminar" ' +
                '           data-toggle="tooltip" data-placement="top" title="Eliminar" data-original-title="Edit">' +
                '           <i class="fas fa-trash"></i>\n' +
                ' </button>';
            @endcan
                return opciones;
        }

        @can('crear.usuarios')
        $("#modalGuardar").on('click', function () {
            $('#modal').modal('show');
            $('#form-usuario')[0].reset();
            $('#actualizar').hide();
            $('#guardar').show();
            $('#eliminar').hide();
            $("#error").hide();
            $("#accion-modal").html('Registrar');
            $("#password").prop('disabled', false);

        });
        $("#guardar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#guardar").prop('disabled', true);
            $.ajax({
                url: '{{url('recurso/users')}}',
                type: 'POST',
                data: $("#form-usuario").serialize(),
            }).done(function (response) {
                $('#form-usuario')[0].reset();
                $('#modal').modal('hide');
                $("#guardar").prop('disabled', false);
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#guardar").prop('disabled', false);
            });

        });
        @endcan

        TABLA.on('click', '.ver', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            console.log(data);

            window.location.href='{{url('usuarios')}}/'+data.id;
            //url del perfil del usuario
        });

        @can('editar.usuarios')
        TABLA.on('click', '.editar', function () {
            $tr = $(this).closest('tr');
            var data = TABLA.row($tr).data();
            $("#modal").modal('show');
            $('#form-usuario')[0].reset();
            $('#actualizar').show();
            $('#eliminar').hide();
            $('#guardar').hide();
            $("#accion-modal").html('Editar');
            $("#id").val(data.id);
            $("#nombre").val(data.nombre);
            $("#correo").val(data.correo);
            $("#rol").val(data.rol[0].id);
            $("#password").prop('disabled', true);

        });
        $("#actualizar").on('click', function () {
            $("#error").hide();
            $('#modal .modal-content').addClass("is-loading");
            $("#actualizar").prop('disabled', true);
            $.ajax({
                url: '{{url('recurso/users')}}/' + $('#id').val(),
                type: 'PUT',
                data: $("#form-usuario").serialize(),
            }).done(function (response) {
                $('#form-usuario')[0].reset();
                $('#modal').modal('hide');
                TABLA.ajax.reload();

                $.notify({
                    icon: 'flaticon-success',
                    title: 'Felicidades',
                    message: response.message,
                }, {
                    type: 'success',
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    time: 1000,
                });
                //return response;
            }).fail(function (error) {
                console.log(error);
                var obj = error.responseJSON.errors;
                $.each(obj, function (key, value) {
                    $("#error").html(value[0]);
                    $("#error").show();
                });


            }).always(function () {
                $('#modal .modal-content').removeClass("is-loading");
                $("#actualizar").prop('disabled', false);
            });

        });
        @endcan


    </script>
@endsection
