<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Auth::routes();

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');;

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/form', 'HomeController@form')->name('form');
Route::post('guardar', 'HomeController@guardar');

Route::get('getStates', 'LocalidadController@getStates');
Route::get('getCities', 'LocalidadController@getCity');
Route::get('getStatesOfCity', 'LocalidadController@getStatesOfCity');
Route::get('getActividades', 'ActividadesController@getActividades');

Route::any('/{any?}', 'HomeController@index')->where('any', '.*')->middleware('auth');
*/
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

//Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/form', 'HomeController@form')->name('form');

Route::get('/empresas', 'HomeController@form')->name('registroEmpresa');
Route::post('/actualizarEmpresa', 'EmpresaController@actualizarEmpresa');
Route::post('/actualizarLogo', 'EmpresaController@actualizarLogo');
Route::get('/dashboardEmpresa', 'EmpresaController@dashboardEmpresa');
Route::post('guardar', 'EmpresaController@store');

Route::get('getStates', 'LocalidadController@getStates');
Route::get('getCities', 'LocalidadController@getCity');
Route::get('getStatesOfCity', 'LocalidadController@getStatesOfCity');
Route::get('getActividades', 'ActividadesController@getActividades');

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'HomeController@index')->name('inicio');
    Route::get('/usuarios/{id}', 'HomeController@usuarios')->name('usuarios')->middleware(['permission:ver.usuarios']);
    Route::get('/usuarios', 'HomeController@usuarios')->name('usuarios')->middleware(['permission:ver.usuarios']);
    Route::get('/roles', 'HomeController@roles')->name('roles')->middleware(['permission:ver.roles']);
    Route::get('/permisos', 'HomeController@permisos')->name('permisos')->middleware(['role:Super Admin']);

    Route::get('/empresas/{id}', 'EmpresaController@empresas');
    Route::get('/empresas', 'HomeController@empresas')->name('empresas')->middleware(['permission:ver.empresas']);
    Route::post('/desactivarEmpresa', 'EmpresaController@desactivarEmpresa');

    Route::get('/perfil', 'HomeController@perfil')->name('perfil');

});


Route::group(['prefix' => 'recurso', 'middleware' => ['auth']], function () {

    Route::apiResource('users', 'Admin\UserController');
    Route::apiResource('roles', 'Admin\RoleController');
    Route::apiResource('permissions', 'Admin\PermissionController');
    Route::apiResource('empresas', 'EmpresaController');

    Route::post('cambiar-pass', 'Admin\UserController@actualizarPass');
    Route::post('cambiar-avatar', 'Admin\UserController@update_profile');


});
