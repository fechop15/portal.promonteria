<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //documentacion https://www.hoclabs.com/2018/01/14/laravel-roles-y-permisos/
        //https://github.com/spatie/laravel-permission

        // create permissions
        Permission::create(['name' => 'ver.usuarios']);
        Permission::create(['name' => 'crear.usuarios']);
        Permission::create(['name' => 'editar.usuarios']);
        Permission::create(['name' => 'eliminar.usuarios']);

        Permission::create(['name' => 'ver.roles']);
        Permission::create(['name' => 'crear.roles']);
        Permission::create(['name' => 'editar.roles']);
        Permission::create(['name' => 'eliminar.roles']);

        Permission::create(['name' => 'ver.categorias']);
        Permission::create(['name' => 'crear.categorias']);
        Permission::create(['name' => 'editar.categorias']);
        Permission::create(['name' => 'eliminar.categorias']);

        Permission::create(['name' => 'ver.empresas']);
        Permission::create(['name' => 'crear.empresas']);
        Permission::create(['name' => 'editar.empresas']);
        Permission::create(['name' => 'eliminar.empresas']);

        // this can be done as separate statements
        $role = Role::create(['name' => 'Admin']);
        $role->givePermissionTo('ver.usuarios');

        $role = Role::create(['name' => 'Super Admin']);
        $role->givePermissionTo(Permission::all());
        //asignar rol a usuario
        $user = User::find(1);
        $user->assignRole('Super Admin');
    }

}
