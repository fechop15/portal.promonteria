<?php


use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user=User::create([
            'name' => 'Fredy Andres Perez Gomez',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456'),
        ]);

    }
}
