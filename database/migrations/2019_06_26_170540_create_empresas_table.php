<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('razonSocial');
            $table->string('nit')
                ->unique();
            $table->string('telefono');
            $table->string('city_id');
            /*
            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities');
            */
            $table->string('direccionEmpresa');
            $table->string('representante');
            $table->integer('anio');
            $table->integer('empleados');
            $table->string('sector');
            $table->string('actividadPrincipal');
            $table->string('actividadSecundaria');
            $table->string('promedioVentas');
            $table->string('totalActivos')->nullable();
            $table->string('operacionesInternacionales');
            $table->string('tipoOperaciones')->nullable();
            $table->string('tieneSucursales')->nullable();
            $table->string('sucursales')->nullable();
            $table->string('comentarios')->nullable();
            $table->string('web')->nullable();
            $table->string('portafolio')->nullable();
            $table->string('logo');
            $table->boolean('activo')->default(true);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
